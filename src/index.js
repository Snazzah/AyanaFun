'use strict';

const path = require('path');

class Fun {
	constructor(base, api) {
		this.base = base;
		this.api = api;

		const { version } = require('../package.json');
		this.version = version;

		this.register = null;
	}

	async onMount() {
		// await this.api.Database.loadModels(path.resolve(__dirname, 'models'));
		this.register = await this.api.Commands.registerAll(path.resolve(__dirname, 'commands'));
	}

	async onUnmount() {
		await this.register.unregisterAll();
	}
}

module.exports = Fun;
