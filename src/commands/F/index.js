'use strict';

const path = require('path');

class F {
	constructor(base) {
		this.base = base;

		this.name = 'f';
		this.alias = ['respect', 'respects', 'rip'];

		this.Respects = null;
	}

	async onMount() {
		const { Respects } = await this.base.Database.loadModels(path.resolve(__dirname, 'models'));
		this.Respects = Respects;
	}

	async execute({ channel }) {
		channel.sendMessage('SoonTM');
	}
}

module.exports = F;

