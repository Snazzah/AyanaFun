'use strict';

module.exports = Schema => ({
	name: 'Respects',
	schema: {
		count: {
			type: Number,
			default: 0,
		},
	},
});
