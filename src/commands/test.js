'use strict';

class Test {
	constructor() {
		this.name = 'test';
	}

	async execute({ channel }) {
		channel.sendMessage(`O hi`);
	}
}

module.exports = Test;
